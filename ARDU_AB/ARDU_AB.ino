// ArduRPG - RPG game for ArduBoy, based on Arduventure engine.

//determine the game
#define GAME_ID 46

#include "globals.h"
#include "songs.h"
#include "menu.h"
#include "game.h"
#include "inputs.h"
#include "text.h"
#include "inventory.h"
#include "items.h"
#include "player.h"
#include "enemies.h"
#include "battles.h"

typedef void (*FunctionPointer) ();

const FunctionPointer PROGMEM mainGameLoop[] =
{
  stateMenuIntro,
  stateMenuMain,
  stateMenuContinue,
  stateMenuNew,
  stateMenuSound,
  stateMenuCredits,
  stateGamePlaying,
  stateGameInventory,
  stateGameEquip,
  stateGameStats,
  stateGameMap,
  stateGameOver,
  showSubMenuStuff,
  showSubMenuStuff,
  showSubMenuStuff,
  showSubMenuStuff,
  walkingThroughDoor,
  stateGameBattle,
  stateGameBoss,
  stateGameIntro,
  stateGameNew,
  stateGameSaveSoundEnd,
  stateGameSaveSoundEnd,
  stateGameSaveSoundEnd,
  stateGameObjects,
  stateGameShop,
  stateGameInn,
  battleGiveRewards,
  stateMenuReboot,
};

int main()
{
  arduboy.mainNoUSB();
  return 0;
}

void setup()
{
  arduboy.boot();
  arduboy.audio.begin();
  ATM.play(titleSong);
  arduboy.setFrameRate(60);

  #if DEBUG
  setPlayer();
  #endif
}

void loop()
{
  if (!(arduboy.nextFrame()))
    return;

  arduboy.pollButtons();
  drawTiles();
  updateEyes();
  
  ((FunctionPointer) pgm_read_word(&mainGameLoop[gameState]))();
  checkInputs();

  if (question)
    drawQuestion();

  if (yesNo)
    drawYesNo();

  if (flashBlack)
    flashScreen(BLACK);     // set in battleStart
  else if (flashWhite)
    flashScreen(WHITE);

  arduboy.display();
}

