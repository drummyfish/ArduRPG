#ifndef MENU_H
#define MENU_H

#include "globals.h"
#include "player.h"
#include "text.h"
#include "songs.h"
#include "people.h"

void stateMenuIntro()
{
  arduboy.fillScreen(0);

  if (globalCounter < 254)
  {
    fillWithSentence(2);
    drawTextBox(31, 17, WHITE);
    globalCounter++;
  }
  else
  {
    sprites.drawSelfMasked(16, 16, titleText, 0);

    if (arduboy.frameCount % 100 < 50)
    {
      fillWithWord(0, 148);
      drawTextBox(49, 45, WHITE);
    }
  }
}

void stateMenuMain()
{
  byte locationMenu = firstGame ? 2 : 8;
  arduboy.fillScreen(0);
  fillWithSentence(0);

  if (!firstGame)
    fillWithWord(1, 1);

  if (arduboy.audio.enabled())
    fillWithWord(25, 6);
  else
    fillWithWord(24, 5);

  drawTextBox(40, locationMenu, WHITE);
  sprites.drawSelfMasked( 32, locationMenu + (cursorY - 2) * 12, font, 44);
  sprites.drawSelfMasked( 90, locationMenu + (cursorY - 2) * 12, font, 45);
}

void stateMenuContinue()
{
  loadGame();
  gameState = STATE_GAME_PLAYING;
  ATM.stop();
}

void stateMenuNew()
{
  setPlayer();
  gameState = STATE_GAME_NEW;
  ATM.play(nameSong);
}

void toggleSound()
{
  if (!arduboy.audio.enabled())
    arduboy.audio.on();
  else
    arduboy.audio.off();

  arduboy.audio.saveOnOff();
  cursorY = STATE_MENU_CONTINUE + firstGame;
}

void stateMenuSound()
{
  arduboy.fillScreen(0);
  // if sound is not enabled, put it ON, otherwise put it OFF
  toggleSound();
  gameState = STATE_MENU_MAIN;
}

void stateMenuCredits()
{
  arduboy.fillScreen(0);
  fillWithSentence(41);
  drawTextBox(18, 11, WHITE);
}

void stateMenuReboot()
{
  arduboy.fillScreen(0);
  fillWithSentence(65);
  drawTextBox(4, 8, WHITE);
  fillWithSentence(66);
  drawTextBox(4, 34, WHITE);

  if (arduboy.pressed(DOWN_BUTTON))
  {
    arduboy.exitToBootloader();
  }
  else if (arduboy.justPressed(A_BUTTON))
  {
    gameState = STATE_MENU_MAIN;
    cursorY = STATE_MENU_CONTINUE + firstGame;
  }
}

#endif
