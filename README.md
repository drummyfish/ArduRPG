# ARDURPG

This is a work-in-progress clone of [Arduventure](https://github.com/TEAMarg/ID-46-Arduventure), an RPG game for [Arduboy](https://arduboy.com/).

This project is motivated by the fact that Arduventure assets are proprietary. A lot of people find this [a huge problem](https://github.com/TEAMarg/ID-46-Arduventure/issues/1) and refuse to play the game. However, the game engine is MIT licensed, so making a completely free clone is mostly a matter of creating free assets.

The newly created assets will all be licensed under CC0. Changes to the engine will probably happen too and will be kept under MIT.

**WARNING**: This repo still contains the original Arduventure assets that are proprietary, so **do not modify or redistribute these**.

Thanks goes to TEAM a.r.g. for creating the engine.
